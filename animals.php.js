<?php
	$update_interval = variable_get('animals_updateInterval', 250);
	$each_animal = _animals_explode(_animals_data());
	foreach ($each_animal as $animal) {
		list($k, $v) = explode('|', $animal);
		$k = trim($k); $v = trim($v);
		$animals_array[] .= "new Array('$k',$v)";
	}
?>
var ak_animals_per_year = new Array( <?php print implode(',', $animals_array); ?> );
var ak_seconds_elapsed = 0;
var ak_seconds_per_year = 365.256366 * 24 * 60 * 60;
function akInitWidget() {
  for (var i = 0; i < ak_animals_per_year.length; i++) {
    $('#ak-animal-'+i).html(ak_animals_per_year[i][0]);
  }
  akRunCounter()
}
function akRunCounter() {
  for (var i = 0; i < ak_animals_per_year.length; i++) {
    var current = Math.floor(ak_animals_per_year[i][1] / ak_seconds_per_year * ak_seconds_elapsed);
    $('#ak-counter-'+i).html(akAddCommas('' + current));
  }
  ak_seconds_elapsed += <?php print ($update_interval / 1000); ?>;
  setTimeout('akRunCounter()', <?php print $update_interval; ?>);
}
function akAddCommas(sValue) {
  var sRegExp = new RegExp('(-?[0-9]+)([0-9]{3})');
  while (sRegExp.test(sValue)) {
    sValue = sValue.replace(sRegExp, '$1,$2');
  }
  return sValue;
}

$(function(){ akInitWidget() });
