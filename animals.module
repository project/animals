<?php
/**
 * @file
 * Animals module implements a simple block to show the number
 * of animals slaughtered for food since the page was opened.
 */

function _animals_version() {
	return '1.0';
}

/**
 * Implementation of hook_perm().
 */
function animals_perm() {
  return array('administer animals');
}

/**
 * Implementation of hook_init().
 */
function animals_init() {
  $ver = _animals_version();
  if (variable_get('animals_version', 0) != $ver) {
    variable_set('animals_version', $ver);
    _animals_apply_settings();
  }
}


/**
 * Implementation of hook_block().
 */
function animals_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks['animals_killed'] = array(
        'info'       => t('Animals Killed'),
        'cache'      => BLOCK_CACHE_GLOBAL,
      );
      return $blocks;
    case 'configure':
      if ($delta == 'animals_killed' && user_access('administer animals')) {
        return _animals_settings_form();
      }
	  return;
    case 'save':
      if ($delta == 'animals_killed') {
        variable_set('animals_data', $edit['animalsData']);
        variable_set('animals_updateInterval', $edit['updateInterval']);
        variable_set('animals_includeCaption', $edit['includeCaption']);
        variable_set('animals_overrideCSS', $edit['overrideCSS']);
        variable_set('animals_transparent', $edit['transparent']);
        variable_set('animals_backgroundColor', $edit['backgroundColor']);
        variable_set('animals_textColor', $edit['textColor']);
        variable_set('animals_countColor', $edit['countColor']);
        _animals_apply_settings();
      }
      return;
    case 'view':
      switch ($delta) {
        case 'animals_killed':
          drupal_add_css(drupal_get_path('module', 'animals') .'/animals.css');
          drupal_add_js(variable_get('animals_js', ''));
          $block = array('subject' => '', 'content' => _animals_get_block());
          break;
      }
      return $block;
  }
}

function _animals_settings_form() {
  $rows = count(_animals_explode(_animals_data()));
  if ($rows < 10) {
    $rows = 10;
  }
  $form = array(
    'overrideCSS' => array(
      '#title' => t('Override CSS'),
      '#type' => 'checkbox',
      '#return_value' => 1,
      '#default_value' => variable_get('animals_overrideCSS', 0),
      '#description' => t('Check this box to use the colors below. Otherwise the default or theme CSS will be used.')
      ),
    'transparent' => array(
      '#title' => t('Transparent Background'),
      '#type' => 'checkbox',
      '#return_value' => 1,
      '#default_value' => variable_get('animals_transparent', 0),
      '#description' => t('Draw with a transparent background. (Ignored if Override CSS is OFF.)')
      ),
    'backgroundColor' => array(
      '#title' => t('Background Color'),
      '#type' => 'textfield',
      '#size' => 10,
      '#element_validate' => array('animals_color_validate'),
      '#default_value' => variable_get('animals_backgroundColor', '#000000'),
      '#description' => t('Background color of the widget. (Ignored if Override CSS is OFF.)')
      ),
    'textColor' => array(
      '#title' => t('Animal Text Color'),
      '#type' => 'textfield',
      '#size' => 10,
      '#element_validate' => array('animals_color_validate'),
      '#default_value' => variable_get('animals_textColor', '#FFFFFF'),
      '#description' => t('Base text color. (Ignored if Override CSS is OFF.)')
      ),
    'countColor' => array(
      '#title' => t('Counter Text Color'),
      '#type' => 'textfield',
      '#size' => 10,
      '#element_validate' => array('animals_color_validate'),
      '#default_value' => variable_get('animals_countColor', '#FFFF00'),
      '#description' => t('Counter color. (Ignored if Override CSS is OFF.)')
      ),
    'includeCaption' => array(
      '#title' => t('Show Caption'),
      '#type' => 'checkbox',
      '#return_value' => 1,
      '#default_value' => variable_get('animals_includeCaption', '1'),
      '#description' => t('Include the informational caption. The "Get This Counter" link cannot be hidden.')
      ),
    'updateInterval' => array(
      '#title' => t('Update Interval (ms)'),
      '#type' => 'textfield',
      '#default_value' => variable_get('animals_updateInterval', 250),
      '#required' => 1,
      '#size' => 10,
      '#maxlength' => 4,
      '#element_validate' => array('animals_interval_validate'),
      '#description' => t('Milliseconds between updates. Must be 100 or greater.')
      ),
	'animalsData' => array(
      '#title' => t('Animals Data'),
      '#type' => 'textarea',
      '#rows' => $rows,
      '#cols' => 40,
      '#default_value' => _animals_data(),
      '#required' => 1,
      '#element_validate' => array('animals_data_validate'),
      '#description' => t('Animals data to use. Please don\'t use invalid data here!')
      ),
  );

  if (module_exists('colorpicker')) {
    $form['backgroundColor']['#type'] = 'colorpicker_textfield';
    $form['backgroundColor']['#colorpicker'] = 'color_picker';
    $form['textColor']['#type'] = 'colorpicker_textfield';
    $form['textColor']['#colorpicker'] = 'color_picker';
    $form['countColor']['#type'] = 'colorpicker_textfield';
    $form['countColor']['#colorpicker'] = 'color_picker';
    $form['color_picker'] = array(
      '#type' => 'colorpicker',
      '#description' => t('Enter a color in RGB format.')
    );
  }

  return $form;
}

/**
 * Element Validators
 */
function animals_data_validate($element) {
	$each_animal = _animals_explode($element['#value']);
	foreach ($each_animal as $animal) {
		if ($animal = trim($animal)) {
			list($k, $v) = explode('|', $animal);
			$k = trim($k); $v = 1 * trim($v);
			if (!preg_match('/^[a-z ]+$/i', $k)) {
				form_set_error('animalsData', t('Animal names must consist of letters and spaces only.'));
				break;
			}
			if (!preg_match('/^\d{7,13}$/i', $v) || $v < 1000000) {
				form_set_error('animalsData', t('Animal kill count values should be 1,000,000 or higher.'));
				break;
			}
		}
	}
}
function animals_interval_validate($element) {
  $v =& $element['#value'];
  if (!preg_match('/^\d+$/i', $v) || $v < 100) {
    form_set_error('updateInterval', t('!val must be a number greater than 100', array('!val' => $v)));
  }
}
function animals_color_validate($element) {
  if (!preg_match('/^#(?:(?:[a-f\d]{3}){1,2})$/i', $element['#value'])) {
    form_set_error($element['#name'], t('!val is not a valid hex color', array('!val' => $element['#value'])));
  }
}

function _animals_apply_settings() {
  variable_set('animals_home', drupal_get_path('module','animals'));

  // Create a custom Javascript file in the files folder
  ob_start();
  include(drupal_get_path('module', 'animals') .'/animals.php.js');
  $data = ob_get_clean();

  $dest = file_directory_path() .'/animals-'. drupal_substr(md5(time()), 22) .'.js';
  if ($oldfile = variable_get('animals_js', 0)) {
    file_delete($oldfile);
  }
  variable_set('animals_js', $dest);
  file_save_data($data, $dest, FILE_EXISTS_REPLACE);

  drupal_set_message(t('Animals Killed settings have been updated.'));
}

/**
 * _animals_get_block
 *
 * Returns the block contents
 *
 */
function _animals_get_block()
{
	if (variable_get('animals_overrideCSS', 0)) {
		$bg = variable_get('animals_transparent', 0) ? 'transparent' : variable_get('animals_backgroundColor', '#000');
		$fg = variable_get('animals_textColor', '#FFF');
		$cc = variable_get('animals_countColor', '#FF0');
		$counterRule = "style=\"color:$cc\"";
		$divRules = "style=\"background-color:$bg;color:$fg\"";
	}

	$each_animal = _animals_explode(_animals_data());
	$x = 0;
	foreach ($each_animal as $animal) {
		list($k, $v) = explode('|', $animal);
		$animal_bits[$k] = $v;
		$divs .= <<<HTML
<div>
	<span class="ak-counter" id="ak-counter-$x"$counterRule></span>
	<span class="ak-animal" id="ak-animal-$x"></span>
</div>
HTML;
		$x++;
	}

	$out = '<div id="animals-killed"'. $divRules .'>'. $divs . '</div>';
	if (variable_get('animals_includeCaption', 1)) {
		$out .= <<<HTML
<p>Number of animals killed in the world by the meat, dairy and egg industries, since you opened this webpage. This does not include the billions of fish and other aquatic animals killed annually.</p>
<p>Based on <a title="Read Global Livestock Production and Health Atlas Statistics" href="http://www.abolitionistapproach.com/media/pdf/2007-glipha-stats.pdf ">2007 statistics</a> from the Food and Agriculture Organization of the United Nations' <a title="Visit Global Livestock Production and Health Atlas Website" href="http://kids.fao.org/glipha/">Global Livestock Production and Health Atlas</a>.</p>
HTML;
	}
	$out .= '<p><a title="Get this counter at BeAutomated.com" href="http://www.beautomated.com/portfolio/kill-counter/">Get this counter &raquo;</a></p>';

	return $out;
}

function _animals_data() {
	//Based on 2007 Food and Agriculture Organization of the United Nations'
	//Global Livestock Production and Health Atlas
	//http://www.abolitionistapproach.com/media/pdf/2007-statistics.pdf.
	return variable_get('animals_data',
		  "buffaloes|23199336\n"	. "camels|1501799\n"
		. "cattle|301275455\n"		. "chickens|49877536490\n"
		. "ducks|2676365000\n"		. "goats|402611664\n"
		. "horses|5018470\n"		. "pigs|1375940758\n"
		. "sheep|564785251\n"		. "turkeys|635382008"
	);
}

function _animals_explode($rawdata) {
	return explode('\n', preg_replace('/[\r\n]+/', '\n', $rawdata));
}
